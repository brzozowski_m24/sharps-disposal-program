# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-01-25 08:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('howdy', '0006_auto_20180125_0909'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='order_shipped',
            field=models.CharField(blank=True, default='', max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='tracking_number',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
