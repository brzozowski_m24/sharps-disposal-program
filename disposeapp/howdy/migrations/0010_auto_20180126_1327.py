# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-01-26 12:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('howdy', '0009_auto_20180126_1302'),
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128)),
                ('slug', models.SlugField(blank=True, default='', max_length=128)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('content', models.TextField()),
                ('script', models.TextField(blank=True, default='')),
                ('style', models.TextField(blank=True, default='')),
            ],
        ),
        migrations.DeleteModel(
            name='ZipCodesOrder',
        ),
    ]
