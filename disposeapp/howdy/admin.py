from django.contrib import admin
from .models import Order
from .models import ZipcodeStats

class OrderAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email_address', 'created_date', 'order_shipped')
    list_filter = ('email_address', 'created_date', 'order_shipped')
    search_fields = ['email_address']

# Register your models here.
admin.site.register(Order, OrderAdmin)

class ZipcodeStatesAdmin(admin.ModelAdmin):
  list_display = ('zip_code', 'count', 'created_at')
  search_fields = ['zip_code']

admin.site.register(ZipcodeStats, ZipcodeStatesAdmin)
