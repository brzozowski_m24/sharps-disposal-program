from django.db import models
from django.utils import timezone

class Order(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    street_address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    zip_code = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    email_address = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    order_shipped = models.CharField(max_length=1, default='', blank=True, null=True)
    tracking_number = models.CharField(max_length=200, blank=True, null=True)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __email__(self):
        return self.email_address

    def __createdDate__(self):
        return self.created_date

    def save(self, *args, **kwargs):
      super(Order, self).save(*args, **kwargs)
      zip_code, created = ZipcodeStats.objects.get_or_create(zip_code=self.zip_code)

      if not created:
          zip_code.count = zip_code.count + 1
          zip_code.save()
      else:
          zip_code.count = 1
          zip_code.save()


class ZipcodeStats(models.Model):
  zip_code = models.CharField(max_length=10, unique=True)
  count = models.IntegerField(default=0)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)
