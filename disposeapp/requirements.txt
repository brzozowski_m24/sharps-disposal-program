amqp==1.4.9
anyjson==0.3.3
billiard==3.3.0.23
celery==3.1.25
certifi==2017.11.5
chardet==3.0.4
cmsplugin-forms-builder==1.1.0
Django==1.9
django-admin-rangefilter==0.3.1
django-appconf==1.0.2
django-classy-tags==0.8.0
django-cms==3.4.5
django-email-extras==0.3.3
django-excel==0.0.10
django-floppyforms==1.7.0
django-forms-builder==0.13.0
django-formtools==2.1
django-ipware==2.0.0
django-mandrill==0.1.0
django-sekizai==0.10.0
django-tables2==1.17.1
django-treebeard==4.2.0
djangocms-admin-style==1.2.7
djangocms-forms==0.2.5
djrill==2.1.0
docopt==0.4.0
et-xmlfile==1.0.1
future==0.15.0
hashids==1.2.0
idna==2.6
jdcal==1.3
jsonfield==2.0.2
kombu==3.0.37
lml==0.0.1
mandrill==1.0.57
odfpy==1.3.6
openpyxl==2.4.9
pipreqs==0.4.9
pyexcel==0.5.7
pyexcel-io==0.5.6
pyexcel-webio==0.1.4
pyftpdlib==1.5.3
pygeoip==0.3.2
python-dateutil==2.6.1
python-gnupg==0.4.1
pytz==2017.3
PyYAML==3.12
requests==2.18.4
six==1.11.0
sphinx-me==0.3
sweetify==1.0.3
tablib==0.12.1
texttable==1.2.1
unicodecsv==0.14.1
Unidecode==1.0.22
urllib3==1.22
WTForms==2.1
xlrd==1.1.0
XlsxWriter==1.0.2
xlwt==1.3.0
yarg==0.1.9
