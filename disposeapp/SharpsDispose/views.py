from django.shortcuts import render, redirect
from .forms import OrderForm
from django.utils import timezone
from dateutil.relativedelta import relativedelta
from django.core.mail import send_mail
import mandrill
import logging
from datetime import timedelta
from django.contrib import messages


logger = logging.getLogger(__name__)
API_KEY = 't1RqghY59LB7Go3tWecAUQ'

def mess_conf(request):
    messages.add_message(request, messages.INFO, 'You have successfuly ordered a container. We will send you link with tracking number.')

def mess_rej(request):
    messages.add_message(request, messages.INFO, 'You can order container after 6 months from previous order.')

def index (request):
    if request.method == 'POST':

        form = OrderForm(request.POST)
        form2 = OrderForm(request.GET)

        if form.is_valid():

            actual_date = timezone.now()
            logger.error(actual_date)
            email_address = form.cleaned_data.get('email_address')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')

            if form2.email_exists(email_address):
                logger.error("email istnieje w bazie")
                created_date = form2.__createdDate__(email_address)
                min_order_date = actual_date - relativedelta(months=6)
                logger.error(min_order_date)

                if min_order_date.date() > created_date.date():
                    logger.error(email_address)
                    created_date = form.cleaned_data.get('created_date')
                    logger.error(created_date)
                    form.save()
                    mandrill_client.messages.send(message = message)
                    logger.error("Tworze xlsx")
                    logger.error("Wysyłam maila")
                    mess_conf(request)
                else:
                    logger.error("nie uplynelo 6 miesiecy")
                    mess_rej(request)
            else:
                logger.error(email_address)
                created_date = form.cleaned_data.get('created_date')
                logger.error(created_date)
                form.save()
                message["to"] = [{
                  'email': email_address,
                  'name': email_address,
                  'type': 'to'
                 }]
                response=mandrill_client.messages.send(message = message)
                print(response)
                logger.error("Tworze xlsx")
                logger.error("Wysyłam maila")
                mess_conf(request)


        return redirect('index')
    else:
        return render (
            request,
            'index.html'
        )

MANDRILL_API_KEY = 't1RqghY59LB7Go3tWecAUQ'
mandrill_client = mandrill.Mandrill(MANDRILL_API_KEY)
message = { 'from_email': 'orders@cardyoursharps.com',
  'from_name': 'Card Your Sharps',
  'subject': "Container order confirmation",
  'text': 'Your container order has been confirmed. We will send you a link with tracking number.'
}
