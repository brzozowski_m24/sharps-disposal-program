from django import forms

from .models import Order

class OrderForm(forms.ModelForm):

    class Meta:
        model = Order
        fields = ('first_name', 'last_name', 'street_address', 'city', 'zip_code', 'state', 'email_address')

    def email_exists( self, email : str ):
            try:
                Order.objects.get(email_address = email)
            except Order.DoesNotExist:
                return False

            return True

    def __createdDate__( self, email : str):
        order = Order.objects.filter(email_address=email).order_by('created_date').last()
        return order.created_date
