#!/usr/bin/python3
from django import db
import openpyxl
import sqlite3
import glob
from ftplib import FTP_TLS
import os
import mandrill


_old_makepasv = FTP_TLS.makepasv
def _new_makepasv(self):
    host,port = _old_makepasv(self)
    host = self.sock.getpeername()[0]
    return host,port
FTP_TLS.makepasv = _new_makepasv

ftp = FTP_TLS()
ftp.connect("cardyoursharps.com")
ftp.login("cardyoursharps", "thwDpE3RLYL7W?8&")
ftp.prot_p()

MANDRILL_API_KEY = 't1RqghY59LB7Go3tWecAUQ'
mandrill_client = mandrill.Mandrill(MANDRILL_API_KEY)


def send_email_confirm(to, track_number):
    message = { 'from_email': 'orders@cardyoursharps.com',
      'from_name': 'Card Your Sharps',
      'subject': "Your order has been shipped",
      'text': "Your order has been shipped. Use tracking information below to track your order: https://wwwapps.ups.com/tracking/tracking.cgi?tracknum=" + track_number
    }
    # print('test')
    # print("To: " + to + "  -  Message: " + message)
    # print("Wysyłam maila z mandrilla")
    message["to"] = [{
      'email': to,
      'name': to,
      'type': 'to'
     }]
    response=mandrill_client.messages.send(message = message)

def send_email_reject(to):
    message = { 'from_email': 'orders@cardyoursharps.com',
      'from_name': 'Card Your Sharps',
      'subject': "Container order information",
      'text': "Unfortunately, container cannot be send to your address."
    }
    message["to"] = [{
      'email': to,
      'name': to,
      'type': 'to'
     }]
    # print("To: " + to + " - Message: " + message)
    # print("Odrzucone")
    response=mandrill_client.messages.send(message = message)


def import_file(book):
    sheet = book.active
    conn = sqlite3.connect('db.sqlite3', timeout=10)
    curs = conn.cursor()
    with conn:
        for row in sheet.iter_rows(min_row=2):
            try:
                order_id = row[7].value
                track_number = row[21].value
                curs.execute('SELECT email_address FROM howdy_order WHERE ID=?', (order_id,))
                result = curs.fetchone()
                to = ''
                if result is not None:
                    to = result[0]
                if track_number is not None:
                    print('ID: ' + str(order_id) + ' -- Track number: ' + str(track_number))
                    curs.execute('UPDATE howdy_order SET order_shipped="1",tracking_number=? WHERE ID=?', (track_number,order_id))
                    if to != '':
                        send_email_confirm(to, track_number)
                else:
                    print('ID: ' + str(order_id) + ' -- no track number')
                    curs.execute('UPDATE howdy_order SET order_shipped="0" WHERE ID=?', (order_id,))
                    if to != '':
                        send_email_reject(to)


            except Exception as e:
                print(e)


        curs.close()
        conn.commit()

for filename in ftp.nlst():
    if(filename.endswith(".xlsx") & filename.startswith("shipping_report")):
        print(filename)
        file = open(filename, 'wb')
        ftp.retrbinary('RETR %s' % filename, file.write)
        file.close()
        book = openpyxl.load_workbook(filename)
        import_file(book)
        os.remove(filename)
        ftp.sendcmd('RNFR ' + filename)
        ftp.sendcmd('RNTO ' + 'ARCHIVE/' + filename)
